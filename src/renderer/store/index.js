import Vue from 'vue';
import Vuex from 'vuex';

import { createPersistedState } from 'vuex-electron';
const fs = require('fs');

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [
    createPersistedState(),
  ],
  strict: process.env.NODE_ENV !== 'production',

  state: {
    tasks: [],
    settings: {
      user: null,
      project: null,
    },
    special: [
      { title: 'Отгулы', time: 0, comment: '' },
      { title: 'Больничный лист', time: 0, comment: '' },
      { title: 'Отпуск', time: 0, comment: '' },
      { title: 'ОБСЗ', time: 0, comment: '' },
      { title: 'Непрофильные задачи', time: 0, comment: '' },
      { title: 'Простои', time: 0, comment: '' },
      { title: 'Дополнительно оплаченное время', time: 0, comment: '' },
    ],
  },
  actions: {
    loadTasks({ commit }) {
      if (!fs.existsSync('./tasks.txt')) return;
      const data = fs.readFileSync('./tasks.txt', 'utf8');
      const tasks = JSON.parse(data);
      commit('setTasks', tasks);
    },

    loadSettings({ commit }) {
      if (!fs.existsSync('./settings.txt')) return;
      const data = fs.readFileSync('./settings.txt', 'utf8');
      const settings = JSON.parse(data);
      commit('setSettings', settings);
    },

    loadSpecial({ commit }) {
      if (!fs.existsSync('./special.txt')) {
        commit('setSpecial', [
          { title: 'Отгулы', time: 0, comment: '' },
          { title: 'Больничный лист', time: 0, comment: '' },
          { title: 'Отпуск', time: 0, comment: '' },
          { title: 'ОБСЗ', time: 0, comment: '' },
          { title: 'Непрофильные задачи', time: 0, comment: '' },
          { title: 'Простои', time: 0, comment: '' },
          { title: 'Дополнительно оплаченное время', time: 0, comment: '' },
        ]);
        commit('saveSpecial');
        return;
      }
      const data = fs.readFileSync('./special.txt', 'utf8');
      const special = JSON.parse(data);
      commit('setSpecial', special);
    },

    saveSettings({ commit }, settings) {
      commit('setSettings', settings);
      commit('saveSettings');
    },

    saveSpecial({ commit }, special) {
      commit('setSpecial', special);
      commit('saveSpecial');
    },

    addTask({ commit, state }, task) {
      const tasks = [...state.tasks, task];
      commit('setTasks', tasks);
      commit('saveTasks');
    },

    removeTask({ commit, state }, task) {
      const tasks = state.tasks.filter(item => item.name !== task.name);
      commit('setTasks', tasks);
      commit('saveTasks');
    },
  },

  mutations: {
    saveTasks(state) {
      const savingTasks = JSON.stringify(state.tasks);
      fs.writeFile('./tasks.txt', savingTasks, () => {});
    },
    saveSettings(state) {
      const savingSettings = JSON.stringify(state.settings);
      fs.writeFile('./settings.txt', savingSettings, () => {});
    },
    saveSpecial(state) {
      const savingSpecial = JSON.stringify(state.special);
      fs.writeFile('./special.txt', savingSpecial, () => {});
    },
    setTasks(state, payload) {
      state.tasks = payload;
    },
    setSettings(state, payload) {
      state.settings = payload;
    },
    setSpecial(state, payload) {
      state.special = payload;
    },
  },
});
