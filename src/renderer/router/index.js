import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/tasks',
      name: 'tasks',
      component: require('@/components/Tasks').default,
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/components/Settings').default,
    },
    {
      path: '/special',
      name: 'special',
      component: require('@/components/SpecialTasks').default,
    },
    {
      path: '*',
      redirect: '/tasks',
    },
  ],
});
